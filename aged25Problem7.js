const data = require('./index (1)')

function nameAndEmailOfAged25(data){
    if(Array.isArray(data)){
        for(let person of data){
            if(person.age === 25){
                return `Name : ${person.name}, email : ${person.email}`;
            }
        }
    }
    else{
        return [];
    }
}
console.log(nameAndEmailOfAged25(data))

module.exports = nameAndEmailOfAged25;