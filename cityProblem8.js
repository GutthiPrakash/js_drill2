const data = require('./index (1)');

function cityAndCountryOfEach(data){
    let cityAndCountry = {
        city : [],
        country : []
    };
    if(Array.isArray(data)){
        for(let person of data){
            cityAndCountry.city.push(person.city)
            cityAndCountry.country.push(person.country)
        }
        return cityAndCountry;
    }
    else{
        return [];
    }
}
console.log(cityAndCountryOfEach(data))

module.exports = cityAndCountryOfEach;