const data = require('./index (1)');

function firstHobbyofEach(data){
    let firstHobby = [];
    if(Array.isArray(data)){
        for(let person of data){
            if(Array.isArray(person.hobbies)){
                firstHobby.push(person.hobbies[0]);
            }
        }
        return firstHobby;
    }
    else{
        return [];
    }
}
console.log(firstHobbyofEach(data))

module.exports = firstHobbyofEach;