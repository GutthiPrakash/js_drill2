const data = require('./index (1)');

function namesOfStudens(data){
    let studentNames = [];
    if(Array.isArray(data)){
        for(let person of data){
            if(person.isStudent === true){
                studentNames.push(person.name);
            }
        }
        return studentNames;
    }
    else{
        return [];
    }
}
console.log(namesOfStudens(data))

module.exports = namesOfStudens;