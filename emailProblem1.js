const data = require('./index (1)');

function getEmailofIndividuals(data){
    let emails = []
    if(Array.isArray(data)){
        for(let person of data){
            emails.push(person.email);
        }
        return emails;
    }
    else{
        return [];
    }
}
console.log(getEmailofIndividuals(data));

module.exports = getEmailofIndividuals;