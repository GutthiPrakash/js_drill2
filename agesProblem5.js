const data = require('./index (1)')

function agesOfIndividuals(data){
    let agesOfAll = [];
    if(Array.isArray(data)){
        for(let person of data){
            agesOfAll.push(person.age);
        }
        return agesOfAll;
    }
    else{
        return [];
    }
}
console.log(agesOfIndividuals(data))

module.exports = agesOfIndividuals;